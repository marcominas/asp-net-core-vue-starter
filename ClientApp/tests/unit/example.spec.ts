import { shallowMount } from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const quote = 'new message';
    const wrapper = shallowMount(HelloWorld, {
      propsData: { quote },
    });
    expect(wrapper.text()).toMatch(quote);
  });
});
