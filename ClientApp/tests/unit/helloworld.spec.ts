import { Wrapper, shallowMount } from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';
import Vue from 'vue';

const name = 'HelloWorld';
const quote = 'new message';
const author = 'new author';

let wrapper: Wrapper<Vue> = Object.assign({}, undefined);

beforeEach(() => {
  wrapper = shallowMount(HelloWorld, {
    propsData: {
      quote,
      author,
    },
  });
});

afterEach(() => {
  wrapper.destroy();
});

describe('HelloWorld.vue', () => {
  it('renders correctly', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });
});

describe('HelloWorld.vue - mount', () => {

  it('renders component', () => {
    expect(wrapper.name()).toMatch(name);
  });

});

describe('HelloWorld.vue - name and props', () => {

  it('check component name', () => {
    expect(wrapper.name()).toMatch(name);
  });

  it('check props.quote when passed', () => {
    expect(wrapper.text()).toMatch(quote);
  });

  it('check props.author when passed', () => {
    expect(wrapper.text()).toMatch(author);
  });

});

describe('HelloWorld.vue - validate markups', () => {

  it('renders the correct markup - quote', () => {
    expect(wrapper.html()).toContain(`${quote}`);
  });

  it('renders the correct markup - author', () => {
    expect(wrapper.html()).toContain(`${author}`);
  });
});
