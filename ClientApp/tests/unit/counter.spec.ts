import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import { counter } from '../../src/store/counter/index';
import Counter from '@/components/Counter.vue';
import Vuex from 'vuex';
import Vue from 'vue';

const getCurrentCount = (value: number) => {
  return `<strong>${value}</strong>`;
};
const localVue = createLocalVue();
const name = 'Counter';

let wrapper: Wrapper<Counter>;
let currentCount = 0;

localVue.use(Vuex);

beforeEach(() => {
  const store = new Vuex.Store({
    modules: { counter },
  });

  Vue.config.silent = true;
  wrapper = shallowMount(Counter, {
    localVue,
    store,
  });
});

afterEach(() => {
  wrapper.destroy();
});

describe('Counter.vue - mount, name and property', () => {

  it('renders correctly', () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('check component name', () => {
    expect(wrapper.name()).toMatch(name);
  });

  it('renders the correct markup - currentCount', () => {
    const getCurrentCountParagraph = (value: number) => {
      return `<p>Current count (Vuex): ${getCurrentCount(value)}</p>`;
    };
    expect(wrapper.html()).toContain(getCurrentCountParagraph(currentCount));
  });

  it('reset `currentCount` when the user clicks it', () => {
    currentCount = 0;
    expect(wrapper.html()).toContain(getCurrentCount(currentCount));
  });

});

describe('Counter.vue - validate actions', () => {

  it('incremet `currentCount` when the user clicks increment', () => {
    const reset = wrapper.find('[test-id="reset"]');
    const increment = wrapper.find('[test-id="increment"]');

    currentCount = 1;
    reset.trigger('click');
    increment.trigger('click');
    expect(wrapper.html()).toContain(getCurrentCount(currentCount));
  });

  it('incremet `currentCount` when the user clicks reset', () => {
    const reset = wrapper.find('[test-id="reset"]');
    const increment = wrapper.find('[test-id="increment"]');

    currentCount = 0;
    increment.trigger('click');
    reset.trigger('click');
    expect(wrapper.html()).toContain(getCurrentCount(currentCount));
  });

});
