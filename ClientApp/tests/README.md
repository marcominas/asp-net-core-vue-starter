# ASP.NET Core Vue Starter unit test

## Project setup

Install dependencies using vue ui:
 * execute `vue ui` to start the Vue.js user interface
 * click on `Plugins` option on left menu
 * click on `add plugin` buttom on top-left of screen
 * type `unit-jest` to find Vue.js plugin and install it

Doing this it will automatically install the `@vue/test-utils` package as an dependency
if not:
 * click on `Dependecies` option on left menu
 * click on `Install dependency` buttom on top-left of screen
 * click on `Development dependecies` buttom. We don't need this on production environment.
 * type `test-utils` to find Vue.js package and install it.

Install dependencies using command:
 * execute `vue add unit-jest`
 * execute `vue add @vue/test-utils`

Tip: try to do via vue ui and update plugins and dependencies if you don't have
much experience with command line.

## Updating package.json

When you install `unit-jest` in your project, the package.json file will be 
updated to allow tests executions but snapshots are not updated by default 
because there is no configuration to do this.

Add following line to allow snapshots updates when necessar:
 * `"test:unit-u": "vue-cli-service test:unit -u",`

## Changing the example.spec.ts

The example that comes with unit-jest expect that the HelloWorld.vue 
component comes with a `msg` property but our instead of comes with a 
`quote` property, so we need change it to avoid error on executing 
test.

## Execute tests

Execute tests and execute + update snapshots respectively
 * `npm run lint && npm run test:unit`
 * `npm run lint && npm run test:unit-u`
