import Vue from 'vue';

// ref => https://medium.com/@farcaller/vue-storybook-typescript-starting-a-new-project-with-best-practices-in-mind-3fc7b3ceae4e
// import '@/plugins/vuetify';

// ref => https://fernandobasso.dev/javascript/unit-testing-vue-vuetify-with-jest-and-vue-test-utils.html
// import Vue from 'vue'

// DON'T DO THIS or you'll have problems like <v-btn :to="..."> rendering
// as <router-link> instead of <a href="..."> on the unit tests.
// See tests/Foo.spec.js to learn how to use Vuetify in the unit tests.

// import Vuetify from 'vuetify';

// Vue.use(Vuetify); // NO, DON'T DO THIS.

// You may not need this. Uncomment only if you see some sort of
// regeneratorRuntime error.
//import 'babel-polyfill';

// So we don't see unnecessary Vue warnings about production.
Vue.config.productionTip = false;
