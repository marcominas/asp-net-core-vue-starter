import { DirectiveOptions } from 'vue';

const directive: DirectiveOptions = {
  inserted(el, node) {
    if (node.value) {
      if (process.env.NODE_ENV === 'test') {
        Object.keys(node.value).forEach((value) => {
          el.setAttribute(`test-${value}`, node.value[value]);
        });
      }
    }
  },
};

export default directive;
