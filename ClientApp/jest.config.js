module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  // preset: 'ts-jest/presets/js-with-babel',
  moduleFileExtensions: ['js', 'jsx', 'json', 'ts', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
        'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest'
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  snapshotSerializers: ['jest-serializer-vue'],
  testMatch: [
    '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ],
  testURL: 'http://localhost/',
  setupFiles: ['./tests/jest-setup.js'],
  transformIgnorePatterns: ['/node_modules(?![\\\\/]vuetify[\\\\/])/', 'node_modules/(?!(vuetify)/)',],
}
